import psycopg2
import sys
from subprocess import check_output

def formatTitle(filename):
    return filename[:len(filename) - 3]

def checkIfNewFile(fileList, file):
    f = file.read()
    for fileName in fileList:
        if fileName in f:
            continue
        else:
            return True, fileName
    return False, None

def insertIntoDB(post, title):
    con = None

    try:
        con = psycopg2.connect("dbname='blog' user='blog' password='beans187'")
        cur = con.cursor()
        cur.execute("INSERT INTO posts (title, body, author, date) VALUES (%s, %s, \'Mathew Robinson\', CURRENT_DATE);", (title, post.read()))
        con.commit()

    except psycopg2.DatabaseError, e:
        if con:
            con.rollback()

        print('Error %s', e)
        sys.exit(1)

    finally:
        if con:
            con.close()




def main():
    meta_data = open("post_meta_data.txt")
    output = check_output(["ls", "posts/"])
    newFile, fileName = checkIfNewFile(output.split('\n'), meta_data)
    if newFile:
        meta_data.close()
        new_meta_data = open("post_meta_data.txt", "w")
        new_meta_data.write(output)
        new_meta_data.close()
        new_post = open("posts/" + fileName)
        insertIntoDB(new_post, formatTitle(fileName))
    else:
        meta_data.close()

main()
