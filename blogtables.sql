CREATE TABLE posts (
	post_id serial NOT NULL PRIMARY KEY,
	title text,
	body text,
	author text
);

ALTER TABLE posts ADD date date;
