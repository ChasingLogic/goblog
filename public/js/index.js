$(document).ready(function(){
    var sticky_navigation_offset_top = $("#navbar").offset().top;
    // LinkedIn pic was just a little too small so had to use relative positioning
    // To make it look right. This is for when we scroll past it so it doesn't
    // dissapear
    var linkedin_pic_trick = $("#linkedin-pic").offset().left;

    $(window).scroll(function () {
            var scroll_top = $(window).scrollTop();
            if (scroll_top > sticky_navigation_offset_top) {
                $("#navbar").css({ 'position': 'fixed', 'top':0, 'left':0 });
                // Hax
                $("#linkedin-pic").css({'position': 'fixed', 'left': linkedin_pic_trick});
            }
            else if (scroll_top <= sticky_navigation_offset_top) {
                $("#navbar").css({'position': 'static', 'top': 'none', 'left': 'none'});
                $("#linkedin-pic").css({'position': 'relative', 'left': -1, 'top': -1});
            }
    });

    //This adds the class to all of our Pre Elements so we can get pretty Syntax Highlighting.
    $("pre").addClass("prettyprint");
});
