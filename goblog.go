package main

import (
	"database/sql"
	"fmt"
	"github.com/codegangsta/martini"
	_ "github.com/lib/pq"
	"github.com/martini-contrib/render"
	"github.com/russross/blackfriday"
	"html/template"
	"net/http"
	"time"
)

const timeFormat = "Jan 2 2006"

type Post struct {
	Title  string
	Body   template.HTML
	Author string
	Date   string
}

func setupDB() *sql.DB {

	db, err := sql.Open("postgres", "dbname=blog user=blog password=beans187 sslmode=disable")
	if err != nil {
		panic(err)
	}

	return db
}

func main() {
	m := martini.Classic()
	m.Use(martini.Static("public/css"))
	m.Use(martini.Static("public/js"))
	m.Use(render.Renderer())
	m.Map(setupDB())

	m.Get("/", loadLandingPage)

	m.RunOnAddr(":80")
}

func loadLandingPage(ren render.Render, r *http.Request, db *sql.DB) {
	rows, err := db.Query("SELECT title, body, author, date FROM posts ORDER BY post_id DESC LIMIT 5;")
	defer rows.Close()

	// OH MY GAWD SHIT BROKE
	if err != nil {
		panic(err)
	}

	posts := []Post{}

	for rows.Next() {

		post := Post{}
		var date time.Time
		var preFormatMarkdown string
		err := rows.Scan(&post.Title, &preFormatMarkdown, &post.Author, &date)

		if err != nil {
			fmt.Println(err)
		}

		post.Date = date.Format(timeFormat)
		post.Body = template.HTML(blackfriday.MarkdownCommon([]byte(preFormatMarkdown)))

		posts = append(posts, post)

	}

	ren.HTML(200, "index", posts)
}
