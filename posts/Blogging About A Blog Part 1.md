### It's Finally Alive.

When I first started programming, I started with Python and Hoping that one day I could write something cool. When I first got the idea to write the code for my own blog I thought man this will be easy. Until I started reading about Django.

Django was my first foray into "Web Development" this cool new thing all the hip kids were talking about. However I was not as advanced as I thought and got lost somewhere between the documentation and tutorials.

### In Comes Go.

When I first found Go I was just coming off of learning C and how cool and impractical it is for WebDev (My primary interest.) Go struck me first with it's simplicity. I'm sure if you're reading this you've seen a thousand permutations of:

    package main

    import "fmt"

    func main() {
        fmt.Println("Hello World")
    }

But coming from C (and loving it) I was struck by how simple this was and how like C/unlike C it was. Needless to say I feel in love.

The biggest issue was while I had learned a lot from my hopping about I never went to school for programming so a lot of the higher level concepts were still tough for me if not completely foreign and one of the best/worst things about Go is that it's a language by programmers in service of software engineering. Which means, it's really practical and everything makes sense you just have to know what you're doing first.

So after about a year of struggling through documentation and frustration I have sucessfully completed this simple blog and in the spirit of open source software I'm going to spend a few posts. (at least 1 a week) talking about how I went from a blank text file to what you see here.

If you aren't familiar with Go I recommmend doing the [tour of Go](tour.golang.org) before reading about these because my goal is not to take you from complete beginner to Gopher but to take you from a beginner programmer who understands the syntax to a Web Developing Gopher.

### A Snippet For Desert

    package main

    import (
        "github.com/codegangsta/martini"
    )

    func main() {
        m := martini.Classic()

        m.Run()
    }

And that's a working webserver. We'll expand on this in the next post.

Until then Google "Golang Martini"
