Now Lets See Some Code
=======================
    package main

    import (
        "github.com/codegangsta/martini"
    )

    func main() {
        m := martini.Classic()

        m.Run()
    }

Break it Down.
====================

    package main

If you met the prerequisite for know some Go then you should know what this is. If you don't then visit [golang.org](golang.org)

    import (
        "github.com/codegangsta/martini"
    )

If you haven't heard about Martini then hold onto your head because it's about to blow your mind. If you've done the [web app tutorial at golang.org](https://golang.org/doc/articles/wiki/) then you know how much of a pain the http library can be sometimes. While I think it's fantastic for being a standard library feature it isn't exactly the best way to get something up fast.

Martini is a fantastic web framework that wraps and simplifies the http standard library. (it does a deal more than that but we'll breeze over most of that for simplicity's sake.) We'll be going over a few of it's features next in the main function.

    func main() {
        m := martini.Classic()

        m.Run()
    }

the m := martini.Classic() gives us the basic martini struct that we'll be using for the entirety of this project. Suffice it to say this is where the magic happens. If you want to go more in depth on how martini works then you can find it on github [here](github.com/codegangsta/martini).

m.Run() is the basic "Start the server" function for Martini and it will serve the files that are in the public/ folder in the root directory of wherever you ran the martini program.

Your Turn
==========

Start your own blog project directory with a public folder and compile the given code with an index.html to see how it all works.

An example index.html if you can't think of anything:

    <h1>Hello World</h1>

### Next Time

We won't be looking at Go at all (or very little). The next couple of posts will be all about Postgresql and what it can do for you.

##### Recommended Reading

Google: [Relational Databases](http://lmgtfy.com/?q=Relational+Databases)
